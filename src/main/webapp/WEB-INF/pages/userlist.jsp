<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户展示页面</title>
</head>
<body>
<div style="margin-left: 25%">
    <table border="2" bordercolor="black" width="300" cellspacing="0" cellpadding="5">
        <tr style="text-align: center">
            <td>id</td>
            <td>姓名</td>
            <td>密码</td>
            <td style="text-align: center" colspan="6">订单信息</td>
        </tr>
        <c:forEach items="${userList}" var="user">
            <tr style="text-align: center">
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.password}</td>
                <%--orderList--%>
                <c:forEach items="${user.orderList}" var="order">
                    <td>${order.oid}</td>
                    <td>${order.ordertime}</td>
                    <td>${order.total}</td>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
