package com.itheima.service.impl;

import com.github.pagehelper.PageHelper;
import com.itheima.domain.Order;
import com.itheima.domain.User;
import com.itheima.mapper.OrderMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author choujin
 * @create 2019/11/24 17:35
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;


    @Override
    public void login(User user) {
        User loginUser = userMapper.findUserByUserNameAndPassword(user);
    }

    @Override
    public List<User> findAllUser() {
        //分页
        PageHelper.startPage(2, 3);
        List<User> userList = userMapper.findAllUserAndOrders();
        for (User user : userList) {
            System.out.println(user);
        }
        return userList;
    }

    @Override
    public void save(User user) {
        userMapper.saveUser(user);
    }



}
