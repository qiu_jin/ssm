package com.itheima.service;

import com.itheima.domain.User;

import java.util.List;

/**
 * @author choujin
 * @create 2019/11/24 17:32
 */
public interface UserService {
    public void login(User user);

    List<User> findAllUser();

    void save(User user);
}
