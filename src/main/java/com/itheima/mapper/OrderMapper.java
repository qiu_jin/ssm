package com.itheima.mapper;

import com.itheima.domain.Order;

import org.apache.ibatis.annotations.*;
import java.util.List;

/**
 * @author choujin
 * @create 2019/11/24 20:00
 */
public interface OrderMapper {
    @Select("select * from tab_orders where uid = #{uid}")
    List<Order> findOrdersByUid(int uid);

}
