package com.itheima.mapper;

import com.itheima.domain.Order;
import com.itheima.domain.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

/**
 * @author choujin
 * @create 2019/11/24 17:37
 */
public interface UserMapper {
    @Select("select * from tab_users where username = #{username} and password = #{password}")
    User findUserByUserNameAndPassword(User user);

    @Select("select * from tab_users ")
    List<User> findAllUser();

    @Insert("insert into tab_users set username=#{username},password=#{password}")
    void saveUser(User user);

    @Select("select * from tab_users")
    @Results({
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "username",property = "username"),
            @Result(column = "password",property = "password"),
            @Result(column = "id",property = "orderList", javaType = List.class,
                    many = @Many(select = "com.itheima.mapper.OrderMapper.findOrdersByUid"))
    })
    List<User> findAllUserAndOrders();

}
