package com.itheima.domain;

import java.util.Date;

/**
 * @author choujin
 * @create 2019/11/24 20:01
 */
public class Order {
    private Integer oid;
    private Date ordertime;
    private Double total;


    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }



    @Override
    public String toString() {
        return "Order{" +
                "oid=" + oid +
                ", ordertime=" + ordertime +
                ", total=" + total +
                '}';
    }
}
