package com.itheima.controller;

import com.itheima.domain.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author choujin
 * @create 2019/11/24 17:29
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public String login(User user){
        userService.login(user);
        return "index";
    }

    @ResponseBody
    @RequestMapping("/findAllUser")
    public ModelAndView findAllUser(){
        List<User> userList = userService.findAllUser();
        ModelAndView mv = new ModelAndView();
        mv.addObject("userList",userList);
        mv.setViewName("userlist");
        return mv;
    }
    @RequestMapping("/saveUser")
    public String saveUser(User user){
        userService.save(user);
        return "redirect:/user/findAllUser";
    }
}
